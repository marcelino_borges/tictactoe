#include <stdio.h>
#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include "main.h"

const int WIDTH = 800, HEIGHT = 600; //Dimens�es da janela

SDL_Surface *introSurface;
SDL_Surface *xSurface;
SDL_Surface *oSurface;
SDL_Surface *playerTurnTextSurface;
SDL_Surface *pcTurnTextSurface;

SDL_Window *window;
SDL_Surface *windowSurface;

bool canUserPlay = true;

struct PlayLocation {
	//Linha da matriz
	int line;
	//Coluna da matrix
	int col;
	//Se houver sido usado por algum player, tera "x" ou "o", se Nao, "-"
	char currentPlayer;
	//Ret�ngulo para printar a imagem da jogada
	SDL_Rect rect;
};

//Posi��es/ret�ngulo de destino de cada img de jogada
PlayLocation	pos00, pos01, pos02,
				pos10, pos11, pos12,
				pos20, pos21, pos22;

PlayLocation boardMatrix[3][3];
int boardCount = 0;

//TODO: Criar um ponteiro para referenciar a ultima posicao jogada e assim desfazer o movimento

//SDL_Texture *xTexture;
//SDL_Texture *oTexture;
//SDL_Texture *bgTexture;

//TODO: Ver como fazer o programa salvar nesse array os endere�os de mem�ria de cada textura e finalizar as 2 funcoes abaixo
//SDL_Texture *allTexturesAdresses[];
//int texturesCount = 0;
//
//void createTexture(SDL_Renderer *renderer, SDL_Surface *surface, SDL_Texture *texture) {
//	SDL_Texture *newTexture = SDL_CreateTextureFromSurface(renderer, surface);
//	allTexturesAdresses[texturesCount] = texture; 
//	texturesCount++;
//}
//
//void destroyAllTextures() {
//	for (int i = 0; i < texturesCount; i++) {
//		SDL_DestroyTexture(allTexturesAdresses[i]);
//	}
//}

void informPlayerTurn() {
	SDL_Rect playerTurnTextRect;
	playerTurnTextRect.x = 20;
	playerTurnTextRect.y = 20;

	if (windowSurface != NULL) {
		SDL_BlitSurface(playerTurnTextSurface, NULL, windowSurface, &playerTurnTextRect);
	}
}

void informPcTurn() {
	SDL_Rect playerTurnTextRect;
	playerTurnTextRect.x = 20;
	playerTurnTextRect.y = 20;

	if (windowSurface != NULL) {
		SDL_BlitSurface(pcTurnTextSurface, NULL, windowSurface, &playerTurnTextRect);
	}
}

int permitUserPlay() {
	canUserPlay = true;
	informPlayerTurn();
	return 1;
}

int forbidUserPlay() {
	canUserPlay = false;
	informPcTurn();
	return 0;
}

int checkKey(SDL_Keycode sym) {
	int done = 0;
	switch(sym) {
			case SDLK_ESCAPE:
				done = 1;
				break;

			////////////////////
			//                //
			//  Player 1 (X)  //
			//   CONTROLES    //
			//                //
			////////////////////
			case SDLK_KP_7:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos00.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 0.0.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos00.rect);
						pos00.currentPlayer = 'x';
						boardMatrix[0][0] = pos00;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 0.0, pois ja a posicao ja esta ocupada.\n");
					}
				}

				SDL_UpdateWindowSurface(window);

				done = 0;
				break;
			case SDLK_KP_8:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos01.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 0.1.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos01.rect);
						pos01.currentPlayer = 'x';
						boardMatrix[0][1] = pos01;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 0.1, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_9:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}
				
				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos02.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 0.2.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos02.rect);
						pos02.currentPlayer = 'x';
						boardMatrix[0][2] = pos02;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 0.2, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_4:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos10.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 1.0.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos10.rect);
						pos10.currentPlayer = 'x';
						boardMatrix[1][0] = pos10;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 1.0, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_5:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos11.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 1.1.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos11.rect);
						pos11.currentPlayer = 'x';
						boardMatrix[1][2] = pos11;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 1.1, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_6:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos12.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 1.2.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos12.rect);
						pos12.currentPlayer = 'x';
						boardMatrix[1][2] = pos12;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 1.2, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_1:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos20.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 2.0.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos20.rect);
						pos20.currentPlayer = 'x';
						boardMatrix[2][0] = pos20;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 2.0, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_2:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos21.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 2.1.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos21.rect);
						pos21.currentPlayer = 'x';
						boardMatrix[2][1] = pos21;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 2.1, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;
			case SDLK_KP_3:
				//Imagem do x
				//Carregar imagem do x
				if (xSurface == NULL) {
					printf("--> SDL Nao conseguiu carregar \"x.png\". Erro: ");
					printf(SDL_GetError());
					printf("\n");
					SDL_Quit();
					//return 1;
				}

				if (canUserPlay) {
					//Checando se essa posicao ja foi ocupada, antes de fazer a jogada
					if (pos22.currentPlayer == '-') {
						printf("Jogada realizada pelo player na posicao 2.2.\n");
						SDL_BlitSurface(xSurface, NULL, SDL_GetWindowSurface(window), &pos22.rect);
						pos22.currentPlayer = 'x';
						boardMatrix[2][2] = pos22;
						forbidUserPlay();
					} else {
						printf("Nao foi possivel realizar jogada na posicao 2.2, pois ja a posicao ja esta ocupada.\n");
					}
				} else {
					printf("Nao foi possivel jogar fora do turno do player.\n");
				}

				SDL_UpdateWindowSurface(window);
				done = 0;
				break;		
	}

	return done;
}

int processEvents() {
	SDL_Event event;

	int done = 0;

	//Checa se existe eventos
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_WINDOWEVENT_CLOSE:
			if (window) {
				SDL_DestroyWindow(window);
				window = NULL;
			}
			done = 1;
			break;
		case SDL_QUIT:
			done = 1;
			break;
		case SDL_KEYDOWN:
			done = checkKey(event.key.keysym.sym);
			break;
		}
	}

	return done;
}

void createMatrixPositions() {
	//Linha 0
	pos00.col = 0;
	pos00.line = 0;
	pos00.rect.x = 120;
	pos00.rect.y = 80;
	pos00.currentPlayer = '-';

	pos01.col = 0;
	pos01.line = 1;
	pos01.rect.x = 355;
	pos01.rect.y = 80;
	pos01.currentPlayer = '-';

	pos02.col = 0;
	pos02.line = 2;
	pos02.rect.x = 590;
	pos02.rect.y = 80;
	pos02.currentPlayer = '-';

	//Linha 1
	pos10.col = 1;
	pos10.line = 0;
	pos10.rect.x = 120;
	pos10.rect.y = 260;
	pos10.currentPlayer = '-';

	pos11.col = 1;
	pos11.line = 1;
	pos11.rect.x = 355;
	pos11.rect.y = 260;
	pos11.currentPlayer = '-';

	pos12.col = 1;
	pos12.line = 2;
	pos12.rect.x = 590;
	pos12.rect.y = 260;
	pos12.currentPlayer = '-';

	//Linha 2
	pos20.col = 2;
	pos20.line = 0;
	pos20.rect.x = 120;
	pos20.rect.y = 430;
	pos20.currentPlayer = '-';

	pos21.col = 2;
	pos21.line = 1;
	pos21.rect.x = 355;
	pos21.rect.y = 430;
	pos21.currentPlayer = '-';

	pos22.col = 2;
	pos22.line = 2;
	pos22.rect.x = 590;
	pos22.rect.y = 430;
	pos22.currentPlayer = '-';
}

int main(int argc, char *argv[]) {
	createMatrixPositions();

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		printf("--> Nao foi possivel inicializar o SDL. Erro: ");
		printf(SDL_GetError());
		printf("\n");
	}

	printf("SDL inicializado com sucesso!\n");

	window = SDL_CreateWindow(
		"JOGO DA VELHA",			//Titulo da janela aqui
		SDL_WINDOWPOS_UNDEFINED,	//Posicao da janela eno eixo x
		SDL_WINDOWPOS_UNDEFINED,	//Posicao da janela eno eixo y
		WIDTH, HEIGHT,				//Largura e Altura da janela
		SDL_WINDOW_ALLOW_HIGHDPI	//Permitindo DPI alto
	);

	windowSurface = NULL;

	windowSurface = SDL_GetWindowSurface(window);

	//Verifica se a janela foi criada com sucesso
	if (window == NULL) {
		printf("--> Janela Nao p�de ser criada. Erro: ");
		printf(SDL_GetError());
		printf("\n");
		return EXIT_FAILURE;
	}

	//Verifica se carregou a biblioteca de imagem
	if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
		printf("--> Nao foi possivel inicializar o SDL_Image para arquivos .PNG. Erro: ");
		printf(SDL_GetError());
		printf("\n");
	}

	//Carregando imagens que ser�o usadas
	SDL_Surface *bgSurface = NULL;
	introSurface = IMG_Load("intro.png");
	bgSurface = IMG_Load("bg.png");
	xSurface = IMG_Load("x.png");
	oSurface = IMG_Load("o.png");
	pcTurnTextSurface = IMG_Load("vezcomputador.png");
	playerTurnTextSurface = IMG_Load("suavez.png");

	if (bgSurface == NULL) {
		printf("--> SDL Nao conseguiu carregar a imagem de fundo. Erro: ");
		printf(SDL_GetError());
		printf("\n");
		SDL_Quit();
		return 1;
	} 
	
	if (oSurface == NULL) {
		printf("--> SDL Nao conseguiu carregar \"o.png\". Erro: ");
		printf(SDL_GetError());
		printf("\n");
		SDL_Quit();
		return 1;
	} 

	if (playerTurnTextSurface == NULL) {
		printf("--> SDL Nao conseguiu carregar a imagem com texto informando a vez do player. Erro: ");
		printf(SDL_GetError());
		printf("\n");
		SDL_Quit();
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	//Imagem de fundo
	SDL_BlitSurface(introSurface, NULL, windowSurface, NULL);
	SDL_Delay(3000);
	SDL_FreeSurface(introSurface);
	SDL_BlitSurface(bgSurface, NULL, windowSurface, NULL);

	SDL_UpdateWindowSurface(window);

	int done = 0;

	//Loop que mant�m a janela aberta
	while (!done) {				
		done = processEvents();
	}

	//Liberando mem�ria para fechar
	SDL_FreeSurface(bgSurface);
	//SDL_FreeSurface(windowSurface);
	bgSurface = NULL;
	windowSurface = NULL;
	SDL_FreeSurface(bgSurface);
	/*SDL_DestroyTexture(xTexture);
	SDL_DestroyTexture(oTexture);
	SDL_DestroyTexture(bgTexture);*/

	SDL_DestroyWindow(window);
	SDL_Quit();

	return EXIT_SUCCESS;
}